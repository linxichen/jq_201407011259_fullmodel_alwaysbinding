%% Try monte carlo integration
load('PEA.mat');
z_old = 1; xxi_old = 0.1634; k_old = 10; b_old = 3
state_old = [k_old,b_old,z_old,xxi_old];
EM = exp([1 log(state_old)]*[coeff_mk coeff_mb coeff_mc]);
u = findcontrol(state_old,EM);
N = 10000;
epsz = normrnd(0,ssigmaz,1,N);
epsxxi = normrnd(0,ssigmaxxi,1,N);
ZZ = zbar*exp(rrhozz*log(z_old/zbar) + rrhozxxi*log(xxi_old/xxibar) + epsz);
XXIXXI = xxibar*exp(rrhoxxiz*log(z_old/zbar) + rrhoxxixxi*log(xxi_old/xxibar) + epsxxi);
scatter(ZZ,XXIXXI);
mk_plus = zeros(N,1); mb_plus = mk_plus; mc_plus = mk_plus;
tic
parfor i = 1:length(ZZ)
    state_plus = [u(1),u(2),ZZ(i),XXIXXI(i)];
    EM_plus = exp([1 log(state_plus)]*[coeff_mk coeff_mb coeff_mc]);
    u_plus = findcontrol(state_plus,EM_plus);
    mk_plus(i) = ( u_plus(8)*(1-ddelta)+(u_plus(8)-u_plus(7))*ttheta*ZZ(i)*k^(ttheta-1)*u_plus(5)^(1-ttheta) )/u_plus(3);
    mb_plus(i) = u_plus(8)/u_plus(3);
    mc_plus(i) = 1/u_plus(3);
end
toc

EMK = mean(mk_plus)
EMB = mean(mb_plus)
EMC = mean(mc_plus)
utilde = findcontrol(state, [EMK EMB EMC]);
utilde(3)
u(3)