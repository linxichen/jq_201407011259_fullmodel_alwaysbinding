function [F,J] = notbindinghour(zkttheta,c,n)
jq_para;
F = -aalpha*c+(1-n)*(1-ttheta)*zkttheta*n^(-ttheta);
J = (ttheta-1)*zkttheta*(ttheta*n^(-ttheta-1)+(1-ttheta)*n^(-ttheta));
end