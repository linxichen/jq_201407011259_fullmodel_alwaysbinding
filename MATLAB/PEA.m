%% Housekeeping
clear all
close all
clc

%% Set the stage
jq_para;
damp_factor = 0.5;
T = 500;
burnin = 50;
maxiter = 200;
tol = 0.0001;
ksim = zeros(1,T);
bsim = ksim;
zsim = ksim;
xxisim = ksim;
mksim = ksim;
mbsim = ksim;
mcsim = ksim;
usim = zeros(8,T);

state = zeros(1,4);
EM = zeros(1,3);
coeff_mk = zeros(4+1,1);
coeff_mb = coeff_mk;
coeff_mc = coeff_mk;

%% Initial guess
kss = 10.0797; bss = 3.6368; zss = 1; xxiss = 0.1634;
coeff_mk(1) = 0.203092;
coeff_mk(2) = -1.045084;
coeff_mk(3) = 0.180915;
coeff_mk(4) = -0.000473/0.0045;
coeff_mk(5) = -0.002222/0.0098;

coeff_mb(1) = 0.191619;
coeff_mb(2) = -1.069579;
coeff_mb(3) = 0.201459;
coeff_mb(4) = -0.000425/0.0045;
coeff_mb(5) = -0.002600/0.0098;

coeff_mc(1) = 0.191619;
coeff_mc(2) = -0.676487;
coeff_mc(3) = 0.022810;
coeff_mc(4) = -0.001372/0.0045;
coeff_mc(5) = -0.000103/0.0098;
ss = [kss bss zss xxiss];
adj = -[0 log(ss)]*[coeff_mk coeff_mb coeff_mb];
coeff_mk(1) = coeff_mk(1) + adj(1);
coeff_mb(1) = coeff_mb(1) + adj(2);
coeff_mc(1) = coeff_mc(1) + adj(3);

%% Simulate shocks
epsz = normrnd(0,ssigmaz,1,T);
epsxxi = normrnd(0,ssigmaxxi,1,T);
zsim = zeros(1,T); xxisim = zeros(1,T);
for t = 2:T
    zsim(t) = rrhozz*zsim(t-1) + rrhozxxi*xxisim(t-1) + epsz(t);
    xxisim(t) = rrhoxxiz*zsim(t-1) + rrhoxxixxi*xxisim(t-1) + epsxxi(t);
end
zsim = zbar*exp(zsim);
xxisim = xxibar*exp(xxisim);

%% Iteration
diff = 10; iter = 0;
while (diff>tol && iter <= maxiter)
%% Simulation endo variables
ksim(1) = 10; bsim(1) = 3;
for t = 1:T
    state(1) = ksim(t);
    state(2)= bsim(t);
    state(3) = zsim(t);
    state(4) = xxisim(t);
    EM = exp([1 log(state)]*[coeff_mk coeff_mb coeff_mc]);
    u = findcontrol(state,EM);
    if (t<T)
        ksim(t+1) = u(1);
        bsim(t+1) = u(2);
    end
    mksim(t) = ( u(8)*(1-ddelta)+(u(8)-u(7))*ttheta*zsim(t)*ksim(t)^(ttheta-1)*u(5)^(1-ttheta) )/u(3);
    mbsim(t) = u(8)/u(3);
    mcsim(t) = 1/u(3);
    usim(:,t) = u;
end

%% Get temp coeff
lmk = log(mksim(burnin+2:end)');
lmb = log(mbsim(burnin+2:end)');
lmc = log(mcsim(burnin+2:end)');
X = [ones(T-1-burnin,1) log(ksim(burnin+1:end-1)') log(bsim(burnin+1:end-1)') log(zsim(burnin+1:end-1)') log(xxisim(burnin+1:end-1)')];
coeff_mk_temp = (X'*X)\(X'*lmk);
coeff_mb_temp = (X'*X)\(X'*lmb);
coeff_mc_temp = (X'*X)\(X'*lmc);

%% Damped update
coeff_mk_new = damp_factor*coeff_mk_temp+(1-damp_factor)*coeff_mk;
coeff_mb_new = damp_factor*coeff_mb_temp+(1-damp_factor)*coeff_mb;
coeff_mc_new = damp_factor*coeff_mc_temp+(1-damp_factor)*coeff_mc;

%% Compute norm
diff = norm([coeff_mk;coeff_mb;coeff_mc]-[coeff_mk_new;coeff_mb_new;coeff_mc_new],2);

%% Update
coeff_mk = coeff_mk_new;
coeff_mb = coeff_mb_new;
coeff_mc = coeff_mc_new;
iter = iter+1;
%% Display something
iter
diff
coeff_mk

end;

%% Euler equation error
load('PEA.mat');
nk = 20; nb = 20; nz = 10; nxxi = 10;
Kgrid = linspace(9,11,nk);
Bgrid = linspace(2,5,nb);
Zgrid = linspace(0.9,1.1,nz);
XXIgrid = linspace(0.15,0.17,nxxi);
EEerror = zeros(nk,nb,nz,nxxi);
for i_k = 1:nk
    for i_b = 1:nb
        for i_z = 1:nz
            for i_xxi = 1:nxxi
                k_old = Kgrid(i_k);
                b_old = Bgrid(i_b);
                z_old = Zgrid(i_z);
                xxi_old = XXIgrid(i_xxi);
                state_old = [Kgrid(i_k),Bgrid(i_b),Zgrid(i_z),XXIgrid(i_xxi)];
                EM = exp([1 log(state_old)]*[coeff_mk coeff_mb coeff_mc]);
                u = findcontrol(state_old,EM);
                k_plus = u(1); b_plus = u(2);
                N = 1000;
                epsz = normrnd(0,ssigmaz,1,N);
                epsxxi = normrnd(0,ssigmaxploxi,1,N);
                ZZ = zbar*exp(rrhozz*log(z_old/zbar) + rrhozxxi*log(xxi_old/xxibar) + epsz);
                XXIXXI = xxibar*exp(rrhoxxiz*log(z_old/zbar) + rrhoxxixxi*log(xxi_old/xxibar) + epsxxi);
                mk_plus = zeros(N,1); mb_plus = mk_plus; mc_plus = mk_plus;
                
                parfor i = 1:length(ZZ)
                    state_plus = [k_plus,b_plus,ZZ(i),XXIXXI(i)];
                    EM_plus = exp([1 log(state_plus)]*[coeff_mk coeff_mb coeff_mc]);
                    u_plus = findcontrol(state_plus,EM_plus);
                    mk_plus(i) = ( u_plus(8)*(1-ddelta)+(u_plus(8)-u_plus(7))*ttheta*ZZ(i)*k_plus^(ttheta-1)*u_plus(5)^(1-ttheta) )/u_plus(3);
                    mb_plus(i) = u_plus(8)/u_plus(3);
                    mc_plus(i) = 1/u_plus(3);
                end
                
                EMK = mean(mk_plus);
                EMB = mean(mb_plus);
                EMC = mean(mc_plus);
                utilde = findcontrol(state_old, [EMK EMB EMC]);
                EEerror(i_k,i_b,i_z,i_xxi) = abs(utilde(3)-u(3));
                disp(EEerror(i_k,i_b,i_z,i_xxi));
                
            end
        end
    end
end


%% Implied policy functions

nk = 10; nb = 3; nz = 3; nxxi = 3;
Kgrid = linspace(5,11,nk);
Bgrid = linspace(2,7,nb);
Zgrid = linspace(0.9,1.1,nz);
XXIgrid = linspace(0.15,0.17,nxxi);
cc = zeros(nk,nb,nz,nxxi);
for i_k = 1:nk
    for i_b = 1:nb
        for i_z = 1:nz
            for i_xxi = 1:nxxi
                state = [Kgrid(i_k) Bgrid(i_b) Zgrid(i_z) XXIgrid(i_xxi)];
                EM = exp([1 log(state)]*[coeff_mk coeff_mb coeff_mc]);
                u = findcontrol(state,EM);
                cc(i_k,i_b,i_z,i_xxi) = u(3);
            end
        end
    end
end
save('PEA.mat');

