//----------------------------------------------------------------
// 1. Declare variables
//----------------------------------------------------------------
var 

// endogenous variables
w           // variable 1
c           // variable 2
n           // variable 3
R          // variable 4
b           // variable 5
d           // variable 6
k           // variable 7
mmu         // variable 8
y           // variable 9
inv         // variable 10
mk         // variable 11
mb           // variable 12
mc         // variable 13
yovern      // variable

// exogenous variables
z          // variable 14
xxi;        // variable 15




//----------------------------------------------------------------
// 2. Exogenous shocks
//----------------------------------------------------------------

varexo epsz epsxxi;

//----------------------------------------------------------------
// 3. Parameters
//----------------------------------------------------------------

parameters 

bbeta ttau aalpha ttheta ddelta xxibar kkappa ssigmaz ssigmaxxi rrhozz rrhozxxi rrhoxxiz rrhoxxixxi eeta

wss   
css          
nss          
Rss      
bss
dss  
kss  
mmuss
zss
xxiss;


//----------------------------------------------------------------
// 4. Calibration 
//----------------------------------------------------------------

bbeta       = 0.9825;
ttau        = 0.3500;
aalpha      = 1.8834;
ttheta      = 0.3600;
ddelta      = 0.0250;
xxibar      = 0.1634;
kkappa      = 0.1460;
ssigmaz     = 0.0045;
ssigmaxxi   = 0.0098;
rrhozz      = 0.9457;
rrhozxxi    = -0.0091;
rrhoxxiz    = 0.0321;
rrhoxxixxi  = 0.9703;

 
//----------------------------------------------------------------
// 5. Steady State
//----------------------------------------------------------------
xxiss = 0.1634;
zss = 1;
Rss = (1/bbeta)*(1-ttau) + ttau;
mmuss = (1-Rss*bbeta)*(1/xxiss)*(Rss-ttau)/(Rss*(1-ttau));
nsskss = ((((1-xxiss*mmuss)/bbeta) - 1 + ddelta )*(1/(ttheta*zss*(1-mmuss))))^(1/(1-ttheta));
kssnss = nsskss^-1;
wss = (1-ttheta)*zss*((kssnss)^ttheta)*(1-mmuss);
bssnss = (kssnss - (zss/xxiss)*(kssnss)^(ttheta))*(Rss-ttau)/(1-ttau);
nssbss = bssnss^-1;
kssbss = kssnss*nssbss;
dssnss = (1-ddelta)*kssnss + zss*kssnss^ttheta - wss -bssnss*(1-Rss^-1)-kssnss;
aux = (wss + bssnss*(1-Rss^-1)+dssnss)*aalpha/wss;
nss = 1/(1 + aux);
kss = kssnss*nss;
bss = bssnss*nss;
dss = dssnss*nss;
css = wss*(1-nss)/aalpha;
yss = zss*(kss^ttheta)*(nss^(1-ttheta));
invss = kss - (1-ddelta)*kss;
mcss = 1/css;
mbss = 1/css;
mkss = (1/css)*(1-ddelta + (1-mmuss)*ttheta*zss*(nss/kss)^(1-ttheta));


//----------------------------------------------------------------
// 6. Model
//----------------------------------------------------------------

model;
  
  // 1. 
  exp(w)/exp(c)  = aalpha/(1-exp(n));
  
  // 2. 
 1/exp(c) = bbeta*((exp(R)-ttau)/(1-ttau))*(1/exp(c(+1)));  

  //3.
exp(w)*exp(n) + exp(b(-1)) - exp(b)/exp(R) + exp(d) - exp(c) = 0;

//4.
 (1-ttheta)*exp(z)*(exp(k(-1))/exp(n))^ttheta = exp(w)*(1/(1-exp(mmu)*(1+2*kkappa*(exp(d)-dss))));

//5.
  bbeta*(exp(c)/exp(c(+1)))*(((1+2*kkappa*(exp(d)-dss)))/((1+2*kkappa*(exp(d(+1))-dss))))*(1-ddelta  + (1-exp(mmu(+1))*(1+2*kkappa*(exp(d(+1))-dss)))*ttheta*exp(z(+1))*(exp(n(+1))/exp(k))^(1-ttheta)) 
+ exp(xxi)*exp(mmu)*(1+2*kkappa*(exp(d)-dss)) = 1;

//6.
  exp(R)*bbeta*(exp(c)/exp(c(+1)))*(((1+2*kkappa*(exp(d)-dss)))/((1+2*kkappa*(exp(d(+1))-dss)))) + exp(xxi)*exp(mmu)*(1+2*kkappa*(exp(d)-dss))*(exp(R)*(1-ttau)/(exp(R)-ttau)) =1;

// 7.
(1-ddelta)*exp(k(-1)) + exp(z)*(exp(k(-1))^(ttheta))*(exp(n)^(1-ttheta)) - exp(w)*exp(n) - exp(b(-1)) + exp(b)/exp(R) - exp(k) - (exp(d)+kkappa*(exp(d)-dss)^2) = 0;

// 8.
exp(xxi)*(exp(k)-exp(b)*((1-ttau)/(exp(R)-ttau))) = exp(z)*(exp(k(-1))^(ttheta))*exp(n)^(1-ttheta);

// 9.
exp(y) = exp(z)*exp(k(-1))^(ttheta)*exp(n)^(1-ttheta);

// 10.
exp(inv) = exp(k)-(1-ddelta)*exp(k(-1));

// 11.
exp(mc) = 1/exp(c);

// 12
exp(mb) = (1/exp(c))*(1/(1+2*kkappa*(exp(d)-dss)));

// 13
exp(mk) = (1/exp(c))*(1/(1+2*kkappa*(exp(d)-dss)))*(1-ddelta + (1-exp(mmu)*(1+2*kkappa*(exp(d)-dss)))*ttheta*exp(z)*(exp(n)/exp(k))^(1-ttheta));

// 14
(exp(z)/zss) = ((exp(z(-1))/zss)^rrhozz)*((exp(xxi(-1))/xxiss)^rrhozxxi)*exp(ssigmaz*epsz);

// 15
(exp(xxi)/xxiss)= ((exp(z(-1))/zss)^rrhoxxiz)*((exp(xxi(-1))/xxiss)^rrhoxxixxi)*exp(ssigmaxxi*epsxxi);

// 16
yovern = y - n;

end;

//----------------------------------------------------------------
// 7. Computation
//----------------------------------------------------------------

initval;
w = log(wss);  
c = log(css);       
n = log(nss);          
R = log(Rss);      
b =log(bss);
d = log(dss);  
k = log(kss);  
inv = log(invss);  
y = log(yss);  
mmu = log(mmuss);
z = log(zss);
xxi = log(xxiss);
mk = log(mkss);
mb = log(mbss);
mc = log(mcss);
epsz = 0;
epsxxi = 0;
end;

check;

steady;
shocks;
var epsz;
stderr 1;
var epsxxi;
stderr 1;
end;

stoch_simul(hp_filter=1600,periods=100000,drop=2000,order = 1,irf=40) y c inv n yovern w z; % compute polices up to 1st order
save