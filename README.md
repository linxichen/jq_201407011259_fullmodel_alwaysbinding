## Overview
This is a project that tries to solve Jermann Quadrini (2012 AER) with minimal modification. 

## Folder and Files
+ /cppcode.cpp: only source code only GCC4.8+ understands.
+ /cuda\_helpers.h: all the rest of helper codes go in here.
+ /Model/: contains LyX and PDF that describe the model.
+ /MATLAB/: contains some codes written in MATLAB
+ /Dynare/: contains some codes written in Dynare to check accuracy of linearization

## Goal
+ Solve it somehow.