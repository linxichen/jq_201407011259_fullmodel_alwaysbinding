/* Includes, system */
#include <iostream>
#include <iomanip>
#include <fstream>

// Includes, Thrust
#include <thrust/functional.h>
#include <thrust/for_each.h>
#include <thrust/sort.h>
#include <thrust/extrema.h>
#include <thrust/tuple.h>
#include <thrust/reduce.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/zip_iterator.h>

/* Includes, cuda */
#include <cublas_v2.h>
#include "cuda_helpers.h"

// Includes, Armadillo
#include "cppcode.h"

// Includes, model specific
#include "model.h"

using namespace std;
using namespace thrust;

// #define M_PI 3.14159265358979323846264338328
#define nk 11 
#define nb 11
#define nz 11 
#define nxxi 11 
#define nm1 501 
#define pk 6
#define pb 6
#define pz 6
#define pxxi 6
#define tol 1e-7
#define maxiter 1000
#define kwidth 1.05
#define bwidth 1.05
#define coeff_llambda 0.5

// Generate Initial Guess from loglinearized solution
void guess_loglinear(const host_vector<double> K, const host_vector<double> B, const host_vector<double> Z, const host_vector<double> XXI, host_vector<double> & DD,host_vector<double> & NN, host_vector<double> & RR, para p, double factor) {
	// Create guesses.
	for (int i_k=0; i_k<nk; i_k++) {
		for (int i_b = 0; i_b < nb; i_b++) {
			for (int i_z = 0; i_z < nz; i_z++) {
				for (int i_xxi = 0; i_xxi < nxxi; i_xxi++) {
					double temp_d = exp(
						   			log(p.dss)	
						           +(-0.7921001)*(log(B[i_b])-log(p.bss))
								   +(1.7976911)*(log(K[i_k])-log(p.kss))
								   +(-0.0059031)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.0119313)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					DD[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi]  = factor*temp_d;

					double temp_n = exp(
						   			log(p.nss)
						           +(-0.624056)*(log(B[i_b])-log(p.bss))
								   +(+1.232885)*(log(K[i_k])-log(p.kss))
								   +(-0.004255)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.011592)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					NN[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi]  = factor*temp_n;

					double temp_R = exp(
						   			log(p.Rss)
						           +(-0.020137)*(log(B[i_b])-log(p.bss))
								   +(+0.023350)*(log(K[i_k])-log(p.kss))
								   +(-0.000041)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.000372)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					RR[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi]  = factor*temp_R;
				};
			};
		};
	};
};

/*
struct findpolicy {
	// Data member
	double *K, *Z, *XXI,  *coeff, *copt, *kopt, *nopt, *mmuopt ;
	double *K_cheby, *Z_cheby, *XXI_cheby;
	int nkout;
	para p;

	// Constructor
	__host__ __device__
	findpolicy(double* K_ptr, double* K_cheby_ptr, double* Z_ptr, double* Z_cheby_ptr, double* XXI_ptr, double* XXI_cheby_ptr, double* coeff_ptr, double* copt_ptr, double* kopt_ptr, double* nopt_ptr, double* mmuopt_ptr, int _nkout, para _p) {
		K = K_ptr;
		Z = Z_ptr;
		XXI = XXI_ptr;
		K_cheby = K_cheby_ptr;
		Z_cheby = Z_cheby_ptr;
		XXI_cheby = XXI_cheby_ptr;
		coeff = coeff_ptr;
		copt = copt_ptr;
		kopt = kopt_ptr;
		nopt = nopt_ptr;
		mmuopt = mmuopt_ptr;
		nkout = _nkout;
		p = _p;
	};

	// Main operator
	__host__ __device__
	void operator()(int index) {
		// Perform ind2sub
		int subs[3];
		int size_vec[3];
		size_vec[0] = nkout;
		size_vec[1] = nz;
		size_vec[2] = nxxi;
		ind2sub(3,size_vec,index,subs);
		int i_k = subs[0];
		int i_z = subs[1];
		int i_xxi = subs[2];

		// Preparation
		double k = K[i_k];
		double z = Z[i_z]; 
		double xxi = XXI[i_xxi];
		state s(k,b,z,xxi,p);

		// Find the current M
		double k_cheby = K_cheby[i_k];
		double z_cheby = Z_cheby[i_z];
		double xxi_cheby = XXI_cheby[i_xxi];
		double arg[3]; 
		arg[0] = k_cheby;
		arg[1] = z_cheby;
		arg[2] = xxi_cheby;
		size_vec[0] = pk+1;
		size_vec[1] = pz+1;
		size_vec[2] = pxxi+1;
		int temp_subs[3];
		double m1 = chebyeval_multi(3,arg,size_vec,temp_subs,coeff);

		// Try not binding first
		control u;
		shadow m(m1);
		u.compute(s,m,p,0);
		if (s.xxi*u.kplus>u.Y) {
			copt[index] = u.c;
			kopt[index] = u.kplus;
			nopt[index] = u.n;
			mmuopt[index] = u.mmu;
		} else {
			u.compute(s,m,p,1);
			copt[index] = u.c;
			kopt[index] = u.kplus;
			nopt[index] = u.n;
			mmuopt[index] = u.mmu;
		};
	};
};
*/

// This functor find new M at each state
struct findnewM
{
	// Data Member
	double *K, *K_cheby;
	double *B, *B_cheby;
	double *Z, *Z_cheby;
	double *XXI, *XXI_cheby;
	double *P;

	double *DD;
	double *DD_new;
	double *DD_coeff;

	double *NN;
	double *NN_new;
	double *NN_coeff;

	double *RR;
	double *RR_new;
	double *RR_coeff;

	double minK, maxK;
	double minB, maxB;
	para p;

	// Construct this object, create util from _util, etc.
	findnewM(double* K_ptr,   double* K_cheby_ptr, 
			 double* B_ptr,   double* B_cheby_ptr,
			 double* Z_ptr,   double* Z_cheby_ptr,
			 double* XXI_ptr, double* XXI_cheby_ptr,
			 double* P_ptr, 
			 double* DD_ptr, double* DD_new_ptr, double* DD_coeff_ptr, 
			 double* NN_ptr, double* NN_new_ptr, double* NN_coeff_ptr, 
			 double* RR_ptr, double* RR_new_ptr, double* RR_coeff_ptr, 
			 double _minK, double _maxK, 
			 double _minB, double _maxB, 
			 para _p)
	{
		K = K_ptr; K_cheby = K_cheby_ptr;
		B = B_ptr; B_cheby = B_cheby_ptr;
		Z = Z_ptr; Z_cheby = Z_cheby_ptr;
		XXI = XXI_ptr; XXI_cheby = XXI_cheby_ptr;
		P = P_ptr;
		DD = DD_ptr; DD_new = DD_new_ptr; DD_coeff = DD_coeff_ptr;
		NN = NN_ptr; NN_new = NN_new_ptr; NN_coeff = NN_coeff_ptr;
		RR = RR_ptr; RR_new = RR_new_ptr; RR_coeff = RR_coeff_ptr;
		minK = _minK; maxK = _maxK;
		minB = _minB; maxB = _maxB;
		p = _p;
	};

	__host__ __device__
	void operator()(int index) {
		int i_xxi = index/(nk*nb*nz);
		int i_z = (index - i_xxi*nk*nb*nz)/(nk*nb);
		int i_b = (index - i_xxi*nk*nb*nz - i_z*nk*nb)/nk;
		int i_k = (index - i_xxi*nk*nb*nz - i_z*nk*nb - i_b*nk);
		double kplus_cheby, bplus_cheby, zplus_cheby, xxiplus_cheby;	
		double Rtilde, llambdatilde, mmutilde, ntilde;
		double EMK, EMB, EMC;

		// Load Variables
		state s(K[i_k],B[i_b],Z[i_z],XXI[i_xxi],p); guess g(DD[index],NN[index],RR[index]);
		control u1;
		control uplus;

		// Case 1: Binding
		u1.compute(s,g,p);
		kplus_cheby = -1 + (u1.kplus-minK)/(maxK-minK)*(2);
		bplus_cheby = -1 + (u1.bplus-minB)/(maxB-minB)*(2);
		// printf("kplus_cheby=%f\n",kplus_cheby);
		// printf("bplus_cheby=%f\n",bplus_cheby);

		// Compute all expectations
		EMK = 0;
		EMB = 0;
		EMC = 0;
		double arg[4]; 
		int size_vec[4];
		size_vec[0] = pk+1;
		size_vec[1] = pb+1;
		size_vec[2] = pz+1;
		size_vec[3] = pxxi+1;
		int temp_subs[4]; // A working space for ind2sub
		arg[0] = kplus_cheby;
		arg[1] = bplus_cheby;
		for (int i_zplus=0; i_zplus<nz; i_zplus++) {
			zplus_cheby = Z_cheby[i_zplus];
			double zplus = Z[i_zplus];
			arg[2] = zplus_cheby;
			for (int i_xxiplus=0; i_xxiplus<nxxi; i_xxiplus++) {
				// All states determined
				xxiplus_cheby = XXI_cheby[i_xxiplus];
				double xxiplus = XXI[i_xxiplus];
				arg[3] = xxiplus_cheby;

				// Find corresponding control variables
				double dplus = chebyeval_multi(4,arg,size_vec,temp_subs,DD_coeff);
				double nplus = chebyeval_multi(4,arg,size_vec,temp_subs,NN_coeff);
				double Rplus = chebyeval_multi(4,arg,size_vec,temp_subs,RR_coeff);
				uplus.compute(state(u1.kplus,u1.bplus,zplus,xxiplus,p),guess(dplus,nplus,Rplus),p);
				EMK += P[i_z+i_xxi*nz+nz*nxxi*i_zplus+nz*nxxi*nz*i_xxiplus]*( uplus.llambda*(1-p.ddelta)+(uplus.llambda-uplus.mmu)*p.ttheta*zplus*pow(u1.kplus/nplus,p.ttheta-1) )/uplus.c;
				EMB += P[i_z+i_xxi*nz+nz*nxxi*i_zplus+nz*nxxi*nz*i_xxiplus]*(uplus.llambda)/uplus.c;
				EMC += P[i_z+i_xxi*nz+nz*nxxi*i_zplus+nz*nxxi*nz*i_xxiplus]*1/uplus.c;
			};
		};
		// printf("ERR=%f\n",ERR);
		
		// Find implied new shadow values
		double temp_R = (p.bbeta*EMC*u1.c);
		Rtilde = (1+temp_R*p.ttau-p.ttau)/temp_R;
		mmutilde = p.bbeta*u1.c*(EMB-EMK/Rtilde)/(1/Rtilde-(1-p.ttau)/(Rtilde-p.ttau))/s.xxi;
		llambdatilde = mmutilde*s.xxi + p.bbeta*u1.c*EMK;
		ntilde = newton(implied_hour(s,u1,p,mmutilde,llambdatilde),0,1,0.5);
		double dtilde = p.dss+(1/llambdatilde-1)/(2*p.kkappa);

		// printf("case1 EM = %f\n",EM);

		// Check whether implied policy functions make sense
		if (
			(u1.c>0) && (Rtilde>p.ttau) && (u1.kplus >= minK) && (u1.kplus <= maxK) && (u1.bplus >= minB) && (u1.bplus <= maxB) && (ntilde>0) && (ntilde<1)
		   )
		{
			DD_new[index] = dtilde;
			NN_new[index] = ntilde;
			RR_new[index] = Rtilde;
			return;
		} else {
			printf("No solution at k=%f, b=%f, z=%f, xxi=%f, d=%f, n=%f, R=%f.\nPolicies are: c=%f,  kplus=%f, bplus=%f, dtilde=%f, ntilde=%f,Rtilde=%f\n====================================================\n",s.k,s.b,s.z,s.xxi,g.d,g.n,g.R,u1.c,u1.kplus,u1.bplus,dtilde,ntilde,Rtilde);
		}; 
	};
};

// This functor yields X, storing basis function values at each state. See Der Hann notes
struct findbasis
{
	// Data member
	double *K_cheby, *B_cheby, *Z_cheby, *XXI_cheby;
	double *X_cheby;

	// Construct this object, create util from _util, etc.
	__host__ __device__
	findbasis(double* K_cheby_ptr, double* B_cheby_ptr, double* Z_cheby_ptr, double* XXI_cheby_ptr, double* X_cheby_ptr)
	{
		K_cheby = K_cheby_ptr; B_cheby = B_cheby_ptr; Z_cheby = Z_cheby_ptr; XXI_cheby = XXI_cheby_ptr;
		X_cheby = X_cheby_ptr;
	};

	__host__ __device__
	void operator()(int index) {
		// Perform sub2ind
		int subs[8];
		int size_vec[8];
		size_vec[0] = nk;
		size_vec[1] = nb;
		size_vec[2] = nz;
		size_vec[3] = nxxi;
		size_vec[4] = pk+1;
		size_vec[5] = pb+1;
		size_vec[6] = pz+1;
		size_vec[7] = pxxi+1;
		ind2sub(8,size_vec,index,subs);
		int i_k   = subs[0];
		int i_b   = subs[1];
		int i_z   = subs[2];
		int i_xxi = subs[3];
		int j_k   = subs[4];
		int j_b   = subs[5];
		int j_z   = subs[6];
		int j_xxi = subs[7];

		double k   = K_cheby[i_k];
		double b   = B_cheby[i_b];
	   	double z   = Z_cheby[i_z];
	   	double xxi = XXI_cheby[i_xxi];
		X_cheby[index] = chebypoly(j_k,k)
			           * chebypoly(j_b,b)
			           * chebypoly(j_z,z)
					   * chebypoly(j_xxi,xxi);
		// printf("Coordinates=(%i, %i, %i, %i, %i, %i), Value=%f\n", i_k, i_z, i_xxi, j_k, j_z, j_xxi, X_cheby[index]);
	};
};	

// This functor find the dampened coefficient vector
struct updatecoeff 
{
	// Data Member
	double *coeff, *coeff_temp, *coeff_new;

	// Constructor
	updatecoeff(double* coeff_ptr, double* coeff_temp_ptr, double* coeff_new_ptr ) {
		coeff = coeff_ptr; coeff_temp = coeff_temp_ptr; coeff_new = coeff_new_ptr;
	};

	__host__ __device__
	void operator()(int index) {
		coeff_new[index] = coeff_llambda*coeff_temp[index] + (1-coeff_llambda)*coeff[index];
	};
};

// This functor calculates the error
struct myMinus {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return abs(get<0>(t)-get<1>(t)) ;
	}
};

// This functor calculates the distance 
struct myDist {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return abs(get<0>(t)-get<1>(t));
	}
};

// Main
int main(int argc, char** argv)
{
	// Reset GPU
	cudaDeviceReset();

	// Set Model Parameters
	para p;
	p.aalpha = 1.8834;
	p.bbeta = 0.9825;
	p.ddelta = 0.025;
	p.ttheta = 0.36;
	p.kkappa = 0.1460;
	p.ttau = 0.3500;
	p.xxibar = 0.1634;
	p.zbar = 1.0;
	p.rrhozz = 0.9457;
	p.rrhoxxiz = 0.0321;
	p.rrhozxxi =-0.0091;
	p.rrhoxxixxi = 0.9703;
	p.var_epsz = 0.0045*0.0045;
	p.var_epsxxi = 0.0098*0.0098;
	p.complete(); // complete all implied p. find S-S

	cout << setprecision(16) << "kss: " << p.kss << endl;
	cout << setprecision(16) << "zss: " << p.zbar << endl;
	cout << setprecision(16) << "xxiss: " <<p.xxibar << endl;
	cout << setprecision(16) << "mkss: " << p.mkss << endl;
	cout << setprecision(16) << "dss: " << p.dss << endl;
	cout << setprecision(16) << "css: " << p.css << endl;
	cout << setprecision(16) << "nss: " << p.nss << endl;
	cout << setprecision(16) << "Rss: " << p.Rss << endl;
	cout << setprecision(16) << "wss: " << p.wss << endl;
	cout << setprecision(16) << "mmuss: " << p.mmuss << endl;
	cout << setprecision(16) << "aalpha: " << p.aalpha << endl;
	cout << setprecision(16) << "tol: " << tol << endl;

	// Select Device
	int num_devices;
	cudaGetDeviceCount(&num_devices);
	if (argc > 1) {
		int gpu = min(num_devices,atoi(argv[1]));
		cudaSetDevice(gpu);
	};
	bool noisy = false;
	if (argc > 2) {
		std::string argv2 = argv[2];
		if (argv2 == "noisy") noisy = true;
	};

	// Only for cuBLAS
	const double alpha = 1.0;
	const double beta = 0.0;

	// Create all STATE, SHOCK grids here
	host_vector<double> h_K(nk); 
	host_vector<double> h_K_cheby(nk); 
	host_vector<double> h_B(nb); 
	host_vector<double> h_B_cheby(nb); 
	host_vector<double> h_Z(nz);
	host_vector<double> h_Z_cheby(nz);
	host_vector<double> h_XXI(nxxi);
	host_vector<double> h_XXI_cheby(nxxi);
	host_vector<double> h_P(nz*nxxi*nz*nxxi,0);
	host_vector<double> h_flag(nk*nb*nz*nxxi, 0); 
	host_vector<double> h_X(nk*nb*nz*nxxi*(1+pk)*(1+pb)*(1+pz)*(1+pxxi)); 
	host_vector<double> h_projector((1+pk)*(1+pb)*(1+pz)*(1+pxxi)*nk*nb*nz*nxxi); 
	host_vector<double> h_DD(nk*nb*nz*nxxi,p.dss); 
	host_vector<double> h_DD_new(nk*nb*nz*nxxi,p.dss); 
	host_vector<double> h_DD_coeff((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_DD_coeff_temp((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_DD_coeff_new((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 

	host_vector<double> h_NN(nk*nb*nz*nxxi,p.nss); 
	host_vector<double> h_NN_new(nk*nb*nz*nxxi,p.nss); 
	host_vector<double> h_NN_coeff((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_NN_coeff_temp((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_NN_coeff_new((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	
	host_vector<double> h_RR(nk*nb*nz*nxxi,p.Rss); 
	host_vector<double> h_RR_new(nk*nb*nz*nxxi,p.Rss); 
	host_vector<double> h_RR_coeff((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_RR_coeff_temp((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	host_vector<double> h_RR_coeff_new((1+pk)*(1+pb)*(1+pz)*(1+pxxi),0.1); 
	// Create capital grid
	double* h_K_cheby_ptr = raw_pointer_cast(h_K_cheby.data());
	chebyroots(nk,h_K_cheby_ptr);
	h_K = h_K_cheby;
	double* h_K_ptr = raw_pointer_cast(h_K.data());
	double minK = (1/kwidth)*p.kss;
	double maxK = (1*kwidth)*p.kss;
	cout << "minK: " << minK << endl;
	cout << "maxK: " << maxK << endl;
	fromchebydomain(minK, maxK, nk, h_K_ptr);

	// Create bond grid
	double* h_B_cheby_ptr = raw_pointer_cast(h_B_cheby.data());
	chebyroots(nb,h_B_cheby_ptr);
	h_B = h_B_cheby;
	double* h_B_ptr = raw_pointer_cast(h_B.data());
	double minB = (1/bwidth)*p.bss;
	double maxB = (1*bwidth)*p.bss;
	cout << "minB: " << minB << endl;
	cout << "maxB: " << maxB << endl;
	fromchebydomain(minB, maxB, nb, h_B_ptr);

	// Create shocks grids
	host_vector<double> h_shockgrids(2*nz);
	double* h_shockgrids_ptr = raw_pointer_cast(h_shockgrids.data());
	double* h_P_ptr = raw_pointer_cast(h_P.data());
	gridgen_fptr chebyspace_fptr = &chebyspace; // select linspace as grid gen
	tauchen_vec(2,nz,4,p.A,p.Ssigma_e,h_shockgrids_ptr,h_P_ptr,chebyspace_fptr);
	for (int i_shock = 0; i_shock < nz; i_shock++) {
		h_Z[i_shock] = p.zbar*exp(h_shockgrids[i_shock+0*nz]);
		h_XXI[i_shock] = p.xxibar*exp(h_shockgrids[i_shock+1*nz]);
	};
	double* h_Z_cheby_ptr = raw_pointer_cast(h_Z_cheby.data());
	double* h_XXI_cheby_ptr = raw_pointer_cast(h_XXI_cheby.data());
	chebyroots(nz,h_Z_cheby_ptr);
	chebyroots(nxxi,h_XXI_cheby_ptr);
	save_vec(h_P,"./fpiter_results/P.csv");

	// Create Initial M generated from linear solution
	guess_loglinear(h_K, h_B, h_Z, h_XXI, h_DD, h_NN, h_RR, p, 1.0);
	save_vec(h_DD,"./fpiter_results/DD_guess.csv");
	save_vec(h_NN,"./fpiter_results/NN_guess.csv");
	save_vec(h_RR,"./fpiter_results/RR_guess.csv");

	// Copy to the device
	device_vector<double> d_K = h_K;
	device_vector<double> d_B = h_B;
	device_vector<double> d_Z = h_Z;
	device_vector<double> d_XXI = h_XXI;

	device_vector<double> d_K_cheby = h_K_cheby;
	device_vector<double> d_B_cheby = h_B_cheby;
	device_vector<double> d_Z_cheby = h_Z_cheby;
	device_vector<double> d_XXI_cheby = h_XXI_cheby;

	device_vector<double> d_P = h_P;
	device_vector<double> d_flag = h_flag;

	device_vector<double> d_X = h_X;
	device_vector<double> d_projector = h_projector;

	device_vector<double> d_DD = h_DD;
	device_vector<double> d_DD_new = h_DD_new;
	device_vector<double> d_DD_coeff = h_DD_coeff;
	device_vector<double> d_DD_coeff_temp = h_DD_coeff_temp;
	device_vector<double> d_DD_coeff_new = h_DD_coeff_new;

	device_vector<double> d_NN = h_NN;
	device_vector<double> d_NN_new = h_NN_new;
	device_vector<double> d_NN_coeff = h_NN_coeff;
	device_vector<double> d_NN_coeff_temp = h_NN_coeff_temp;
	device_vector<double> d_NN_coeff_new = h_NN_coeff_new;

	device_vector<double> d_RR = h_RR;
	device_vector<double> d_RR_new = h_RR_new;
	device_vector<double> d_RR_coeff = h_RR_coeff;
	device_vector<double> d_RR_coeff_temp = h_RR_coeff_temp;
	device_vector<double> d_RR_coeff_new = h_RR_coeff_new;

	// Obtain device pointers
	double* d_K_ptr = raw_pointer_cast(d_K.data());
	double* d_B_ptr = raw_pointer_cast(d_B.data());
	double* d_Z_ptr = raw_pointer_cast(d_Z.data());
	double* d_XXI_ptr = raw_pointer_cast(d_XXI.data());

	double* d_K_cheby_ptr = raw_pointer_cast(d_K_cheby.data());
	double* d_B_cheby_ptr = raw_pointer_cast(d_B_cheby.data());
	double* d_Z_cheby_ptr = raw_pointer_cast(d_Z_cheby.data());
	double* d_XXI_cheby_ptr = raw_pointer_cast(d_XXI_cheby.data());

	double* d_P_ptr = raw_pointer_cast(d_P.data());
	double* d_flag_ptr = raw_pointer_cast(d_flag.data());
	double* d_X_ptr = raw_pointer_cast(d_X.data());

	double* d_DD_ptr = raw_pointer_cast(d_DD.data());
	double* d_DD_new_ptr = raw_pointer_cast(d_DD_new.data());
	double* d_DD_coeff_ptr= raw_pointer_cast(d_DD_coeff.data());
	double* d_DD_coeff_temp_ptr= raw_pointer_cast(d_DD_coeff_temp.data());
	double* d_DD_coeff_new_ptr= raw_pointer_cast(d_DD_coeff_new.data());

	double* d_NN_ptr = raw_pointer_cast(d_NN.data());
	double* d_NN_new_ptr = raw_pointer_cast(d_NN_new.data());
	double* d_NN_coeff_ptr= raw_pointer_cast(d_NN_coeff.data());
	double* d_NN_coeff_temp_ptr= raw_pointer_cast(d_NN_coeff_temp.data());
	double* d_NN_coeff_new_ptr= raw_pointer_cast(d_NN_coeff_new.data());

	double* d_RR_ptr = raw_pointer_cast(d_RR.data());
	double* d_RR_new_ptr = raw_pointer_cast(d_RR_new.data());
	double* d_RR_coeff_ptr= raw_pointer_cast(d_RR_coeff.data());
	double* d_RR_coeff_temp_ptr= raw_pointer_cast(d_RR_coeff_temp.data());
	double* d_RR_coeff_new_ptr= raw_pointer_cast(d_RR_coeff_new.data());

	double* d_projector_ptr = raw_pointer_cast(d_projector.data());

	// Firstly a virtual index array from 0 to nk*nk*nz
	counting_iterator<int> begin(0);
	counting_iterator<int> end(nk*nb*nz*nxxi*(1+pk)*(1+pb)*(1+pz)*(1+pxxi));

	// Start Timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,NULL);
	
	// Step.1 Has to start with this command to create a handle
	cublasHandle_t handle;

	// Step.2 Initialize a cuBLAS context using Create function,
	// and has to be destroyed later
	cublasCreate(&handle);
	
	// Find the projector matrix once and for all
	double* h_X_ptr = raw_pointer_cast(h_X.data());
	double* h_projector_ptr = raw_pointer_cast(h_projector.data());
	thrust::for_each(
		begin,
		end,
		findbasis(d_K_cheby_ptr, d_B_cheby_ptr, d_Z_cheby_ptr, d_XXI_cheby_ptr, d_X_ptr)
	);
	h_X = d_X;
	findprojector(h_X_ptr, nk*nb*nz*nxxi, (1+pk)*(1+pb)*(1+pz)*(1+pxxi), h_projector_ptr);
	d_projector = h_projector; // Copy host projector to device

	// Regress M_guess on basis to find initial coefficient
	// This is doing coeff = (X'X)*X'*M
	cublasDgemv(
			handle,	// cuBlas handle
			CUBLAS_OP_N, // N means don't transpose A
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
			nk*nb*nz*nxxi, // # of col in matrix A
			&alpha, // just 1
			d_projector_ptr, // pointer to matrix A stored in column-major format
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
			d_DD_ptr, // pointer to x
			1, // stride of x, usually 1
			&beta, // usually zero
			d_DD_coeff_ptr, // pointer to y
			1 // stride of y
			);
	cublasDgemv(
			handle,	// cuBlas handle
			CUBLAS_OP_N, // N means don't transpose A
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
			nk*nb*nz*nxxi, // # of col in matrix A
			&alpha, // just 1
			d_projector_ptr, // pointer to matrix A stored in column-major format
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
			d_NN_ptr, // pointer to x
			1, // stride of x, usually 1
			&beta, // usually zero
			d_NN_coeff_ptr, // pointer to y
			1 // stride of y
			);
	cublasDgemv(
			handle,	// cuBlas handle
			CUBLAS_OP_N, // N means don't transpose A
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
			nk*nb*nz*nxxi, // # of col in matrix A
			&alpha, // just 1
			d_projector_ptr, // pointer to matrix A stored in column-major format
			(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
			d_RR_ptr, // pointer to x
			1, // stride of x, usually 1
			&beta, // usually zero
			d_RR_coeff_ptr, // pointer to y
			1 // stride of y
			);

	// Main iterations
	double diff = 10; int iter = 0;
	while ((diff>tol)&&(iter<maxiter)){
		// Find the current M at each state. Does y = A*x/ M = X*coeff
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				nk*nb*nz*nxxi, // # of row in matrix A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of col in matrix A
				&alpha, // just 1
				d_X_ptr, // pointer to matrix A stored in column-major format
				nk*nb*nz*nxxi, // leading dimesnion of array to store A, usually # of rows
				d_DD_coeff_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_DD_ptr, // pointer to y
				1 // stride of y
				);
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				nk*nb*nz*nxxi, // # of row in matrix A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of col in matrix A
				&alpha, // just 1
				d_X_ptr, // pointer to matrix A stored in column-major format
				nk*nb*nz*nxxi, // leading dimesnion of array to store A, usually # of rows
				d_NN_coeff_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_NN_ptr, // pointer to y
				1 // stride of y
				);
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				nk*nb*nz*nxxi, // # of row in matrix A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of col in matrix A
				&alpha, // just 1
				d_X_ptr, // pointer to matrix A stored in column-major format
				nk*nb*nz*nxxi, // leading dimesnion of array to store A, usually # of rows
				d_RR_coeff_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_RR_ptr, // pointer to y
				1 // stride of y
				);

		// Based on current M(k,z,xxi), find implied new M
		thrust::for_each(
				make_counting_iterator(0),
				make_counting_iterator(nk*nb*nz*nxxi),
				findnewM(d_K_ptr, d_K_cheby_ptr, d_B_ptr, d_B_cheby_ptr, d_Z_ptr, d_Z_cheby_ptr, d_XXI_ptr, d_XXI_cheby_ptr, d_P_ptr, d_DD_ptr, d_DD_new_ptr, d_DD_coeff_ptr, d_NN_ptr, d_NN_new_ptr, d_NN_coeff_ptr, d_RR_ptr, d_RR_new_ptr, d_RR_coeff_ptr, minK, maxK, minB, maxB, p)
				);

		// Regress new M on basis to find temporary coefficient
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
				nk*nb*nz*nxxi, // # of col in matrix A
				&alpha, // just 1
				d_projector_ptr, // pointer to matrix A stored in column-major format
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
				d_DD_new_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_DD_coeff_temp_ptr, // pointer to y
				1 // stride of y
				);
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
				nk*nb*nz*nxxi, // # of col in matrix A
				&alpha, // just 1
				d_projector_ptr, // pointer to matrix A stored in column-major format
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
				d_NN_new_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_NN_coeff_temp_ptr, // pointer to y
				1 // stride of y
				);
		cublasDgemv(
				handle,	// cuBlas handle
				CUBLAS_OP_N, // N means don't transpose A
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // # of row in matrix A
				nk*nb*nz*nxxi, // # of col in matrix A
				&alpha, // just 1
				d_projector_ptr, // pointer to matrix A stored in column-major format
				(1+pk)*(1+pb)*(1+pz)*(1+pxxi), // leading dimesnion of array to store A, usually # of rows
				d_RR_new_ptr, // pointer to x
				1, // stride of x, usually 1
				&beta, // usually zero
				d_RR_coeff_temp_ptr, // pointer to y
				1 // stride of y
				);

		// Update coefficient with dampening
		thrust::for_each(
			make_counting_iterator(0),
			make_counting_iterator((1+pk)*(1+pb)*(1+pz)*(1+pxxi)),
			updatecoeff(d_DD_coeff_ptr,d_DD_coeff_temp_ptr,d_DD_coeff_new_ptr)
		);
		thrust::for_each(
			make_counting_iterator(0),
			make_counting_iterator((1+pk)*(1+pb)*(1+pz)*(1+pxxi)),
			updatecoeff(d_NN_coeff_ptr,d_NN_coeff_temp_ptr,d_NN_coeff_new_ptr)
		);
		thrust::for_each(
			make_counting_iterator(0),
			make_counting_iterator((1+pk)*(1+pb)*(1+pz)*(1+pxxi)),
			updatecoeff(d_RR_coeff_ptr,d_RR_coeff_temp_ptr,d_RR_coeff_new_ptr)
		);

		// Compute difference between coefficient vectors
		double diffd = transform_reduce(
			make_zip_iterator(make_tuple(d_DD_coeff.begin(),d_DD_coeff_new.begin())),
			make_zip_iterator(make_tuple(d_DD_coeff.end(),d_DD_coeff_new.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffn = transform_reduce(
			make_zip_iterator(make_tuple(d_NN_coeff.begin(),d_NN_coeff_new.begin())),
			make_zip_iterator(make_tuple(d_NN_coeff.end(),d_NN_coeff_new.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffR = transform_reduce(
			make_zip_iterator(make_tuple(d_RR_coeff.begin(),d_RR_coeff_new.begin())),
			make_zip_iterator(make_tuple(d_RR_coeff.end(),d_RR_coeff_new.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		diff = max(diffd,max(diffn,diffR));

		// Replace old coefficient with new
		d_DD_coeff = d_DD_coeff_new;
		d_NN_coeff = d_NN_coeff_new;
		d_RR_coeff = d_RR_coeff_new;

		iter++;
		printf("=======================================================\n=== Iteration No. %i finished \n=======================================================\n",iter);
		printf("=======================================================\n=== Diff=%f \n=======================================================\n",diff);
	};

	//==========cuBLAS stuff ends=======================
	// Step.3 Destroy the handle.
	cublasDestroy(handle);

	// Stop Timer
	cudaEventRecord(stop,NULL);
	cudaEventSynchronize(stop);
	float msecTotal = 0.0;
	cudaEventElapsedTime(&msecTotal, start, stop);

	// Compute and print the performance
	float msecPerMatrixMul = msecTotal;
	cout << "Time= " << msecPerMatrixMul << " msec, iter= " << iter << endl;

	/*
	// Find policy
	int nkout = 50001;
	host_vector<double> h_Kgrid(nkout);
	host_vector<double> h_Kgrid_cheby(nkout);
	chebyroots(nkout,h_Kgrid_cheby.data());
	chebyspace(minK,maxK,nkout,h_Kgrid.data());
	device_vector<double> d_Kgrid = h_Kgrid;
	device_vector<double> d_Kgrid_cheby = h_Kgrid_cheby;
	device_vector<double> d_copt(nkout*nz*nxxi);
	device_vector<double> d_kopt(nkout*nz*nxxi);
	device_vector<double> d_nopt(nkout*nz*nxxi);
	device_vector<double> d_mmuopt(nkout*nz*nxxi);
	double* d_Kgrid_ptr = raw_pointer_cast(d_Kgrid.data());
	double* d_Kgrid_cheby_ptr = raw_pointer_cast(d_Kgrid_cheby.data());
	double* d_copt_ptr = raw_pointer_cast(d_copt.data());
	double* d_kopt_ptr = raw_pointer_cast(d_kopt.data());
	double* d_nopt_ptr = raw_pointer_cast(d_nopt.data());
	double* d_mmuopt_ptr = raw_pointer_cast(d_mmuopt.data());

	thrust::for_each(
			make_counting_iterator(0),
			make_counting_iterator(nkout*nz*nxxi),
			findpolicy(d_Kgrid_ptr, d_Kgrid_cheby_ptr, d_Z_ptr, d_Z_cheby_ptr, d_XXI_ptr, d_XXI_cheby_ptr,d_coeff_ptr,d_copt_ptr,d_kopt_ptr,d_nopt_ptr,d_mmuopt_ptr,nkout,p)
			);

	// Copy back to host and print to file
	h_coeff = d_coeff;
	h_M_new = d_M_new;
	h_M = d_M;
	host_vector<double> h_copt = d_copt;
	host_vector<double> h_kopt = d_kopt;
	host_vector<double> h_nopt = d_nopt;
	host_vector<double> h_mmuopt = d_mmuopt;
    save_vec(h_Kgrid,"./fpiter_results/Kgrid.csv");
    save_vec(h_Z,"./fpiter_results/Zgrid.csv");
    save_vec(h_XXI,"./fpiter_results/XXIgrid.csv");
    save_vec(h_P,"./fpiter_results/P.csv");
    save_vec(h_copt,"./fpiter_results/copt.csv");
    save_vec(h_kopt,"./fpiter_results/kopt.csv");
    save_vec(h_nopt,"./fpiter_results/nopt.csv");
    save_vec(h_mmuopt,"./fpiter_results/mmuopt.csv");
    save_vec(h_coeff,"./fpiter_results/coeff.csv");
    save_vec(h_M,"./fpiter_results/M.csv");
    save_vec(h_M_new,"./fpiter_results/M_new.csv");
	p.exportmatlab("./MATLAB/fpiter_para.m");

	// Save accuracy controls
	std::ofstream fileout("./fpiter_results/accuracy.m", std::ofstream::trunc);
	fileout << std::setprecision(16) << "pk=" << pk << ";"<< std::endl;
	fileout << std::setprecision(16) << "pz=" << pz<< ";"<< std::endl;
	fileout << std::setprecision(16) << "pxxi=" << pxxi<< ";"<< std::endl;
	fileout.close();

	*/
    return 0;
};


