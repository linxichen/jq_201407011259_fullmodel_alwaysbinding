clear all
clc 
close all

%
ddebug = csvread('debug.csv');
nm = nthroot(size(ddebug),3);
nm = max(nm);
ddebug = reshape(ddebug,[nm nm nm]);
x = linspace(0,nm-1,nm);
y = linspace(0,nm-1,nm);
z = linspace(0,nm-1,nm);
list = find(ddebug==1);
for i = 1:size(list)
    [i_x,i_y,i_z] = ind2sub([nm nm nm],list(i));
    xx(i) = x(i_x);
    yy(i) = y(i_y);
    zz(i) = z(i_z);
end

scatter3(xx,yy,zz)
xlabel('mk')
ylabel('mb')
zlabel('mc')