function [A,boverR,mmu,R,c,d,n,kplus,bplus] = survival(k,b,z,xxi,mkovermb,mbovermc,mc)
    dbar = 0.1147858823737546;
    kkappa = 0.1460;
    ttheta = 0.36;
    aalpha = 1.8834;
    ddelta = 0.025;
    ttau = 0.35;
    mb = mbovermc*mc;
    mk = mkovermb*mb;
    
    c = 1/mc;
    llambda = mb/mc;
    d = dbar + (1/llambda-1)/(2*kkappa);
    G = (1-ttheta)*k*(mk*c/llambda-1+ddelta)/(ttheta*aalpha*c);
    n = G/(1+G);
    Y = z*(k^ttheta)*(n^(1-ttheta));
    mmu = llambda*( 1-aalpha*c*G/((1-ttheta)*Y) );
    w = aalpha*c/(1-n);
    boverR = w*n+b+d-c;
    kplus = (1-ddelta)*k + Y - c - kkappa*(d-dbar)^2;
    A = (kplus-Y/xxi)/boverR;
    R = ttau*A/(ttau+A-1);
    bplus = boverR*R;
end