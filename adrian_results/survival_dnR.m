function [mk,mb,mc,c,kplus,bplus] = survival_dnR(k,b,z,xxi,d,n,R)
    dbar = 0.1147858823737546;
    kkappa = 0.1460;
    ttheta = 0.36;
    aalpha = 1.8834;
    ddelta = 0.025;
    ttau = 0.35;
    
    Y = z*k^ttheta*n^(1-ttheta);
    nomin = b+d+(aalpha*n/(1-n)-1)*( (1-ddelta)*k+Y*(1-1/xxi)-kkappa*(d-dbar)^2 );
    denom =1/R - (1-aalpha*n/(1-n))*(1-ttau)/(R-ttau);
    bplus = nomin/denom;
    kplus = Y/xxi + bplus*(1-ttau)/(R-ttau);
    c = (1-ddelta)*k+Y-kkappa*(d-dbar)^2 - kplus;
    llambda = 1/(1+2*kkappa*(d-dbar));
    MPN = (1-ttheta)*Y/n;
    mmu = llambda*(1-aalpha*c/(1-n)/MPN);
    mk = 1/c*(llambda*(1-ddelta)+(llambda-mmu)*ttheta*z*(k/n)^(ttheta-1));
    mb = 1/c*llambda;
    mc = 1/c;
   
end