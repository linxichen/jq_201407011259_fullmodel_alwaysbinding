clear all
clc 
close all

nd = 70;
nn = 70;
nc = 70;


mmummu = zeros(nc,nd,nn);
RR = mmummu;
kpluskplus = mmummu;
bplusbplus = mmummu;
mk = RR;
mb = RR;
mc = RR;
GG = RR;

DD = linspace(0.10,0.12,nd);
NN = linspace(0.2,0.4,nn);
CC = linspace(0.7,0.9,nc);

for i_d = 1:nd
    for i_n = 1:nn
        for i_c = 1:nc
            c = CC(i_c);
            d = DD(i_d);
            n = NN(i_n);
            
            [GG(i_c,i_d,i_n),mk(i_c,i_d,i_n),mb(i_c,i_d,i_n),mc(i_c,i_d,i_n),RR(i_c,i_d,i_n),kpluskplus(i_c,i_d,i_n),bplusbplus(i_c,i_d,i_n)]...
            = survival_cdn(10.0797,3.6368,1.0,0.1634,c,d,n);
        end
    end
end

%%
d_index = floor(nd/2);
figure
surf(NN,CC,squeeze(kpluskplus(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('capital tomorrow')

figure
surf(NN,CC,squeeze(bplusbplus(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('debt tomorrow')

figure
surf(NN,CC,squeeze(RR(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('Interest Rate')

figure
surf(NN,CC,squeeze(mk(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mk')

figure
surf(NN,CC,squeeze(mb(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mb')

figure
surf(NN,CC,squeeze(mc(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mc')
%%
figure
surf(DD,CC,squeeze(kpluskplus(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('capital tomorrow')

figure
surf(DD,CC,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('debt tomorrow')

figure
surf(DD,CC,squeeze(RR(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('Interest Rate')

figure
surf(DD,CC,squeeze(mk(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('mk')

figure
surf(DD,CC,squeeze(mb(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('mb')

figure
surf(DD,CC,squeeze(mc(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('mc')

figure
surf(DD,CC,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('debt tomorrow')

figure
surf(DD,CC,squeeze(RR(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('Interest')

%% 
list = RR>0.35;
RR_filter = list.*RR;
figure
surf(DD,CC,squeeze(RR_filter(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('Interest Survived')