clear all
clc 
close all

nd = 70;
nn = 70;
nR = 70;


mmummu = zeros(nd,nn,nR);
cc = mmummu;
kpluskplus = mmummu;
bplusbplus = mmummu;
mk = cc;
mb = cc;
mc = cc;

DD = linspace(0.10,0.12,nd);
NN = linspace(0.2,0.4,nn);
RR = linspace(1.001,1.12,nR);

for i_d = 1:nd
    for i_n = 1:nn
        for i_R = 1:nR
            d = DD(i_d);
            n = NN(i_n);
            R = RR(i_R);
            [mk(i_d,i_n,i_R),mb(i_d,i_n,i_R),mc(i_d,i_n,i_R),cc(i_d,i_n,i_R),kpluskplus(i_d,i_n,i_R),bplusbplus(i_d,i_n,i_R)]...
            = survival_dnR(10.0797,3.6368,1.0,0.1634,d,n,R);
        end
    end
end

%%
figure
surf(RR,DD,squeeze(kpluskplus(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('capital tomorrow')

figure
surf(RR,DD,squeeze(bplusbplus(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('debt tomorrow')

figure
surf(RR,DD,squeeze(cc(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('consumption')

figure
surf(RR,DD,squeeze(mk(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('mk')

figure
surf(RR,DD,squeeze(mb(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('mb')

figure
surf(RR,DD,squeeze(mc(:,end,:)));
xlabel('dividend')
ylabel('interest rate')
title('mc')
%%
figure
surf(NN,DD,squeeze(kpluskplus(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('capital tomorrow')

figure
surf(NN,DD,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('debt tomorrow')

figure
surf(NN,DD,squeeze(cc(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('consumption')

figure
surf(NN,DD,squeeze(mk(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('mk')

figure
surf(NN,DD,squeeze(mb(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('mb')

figure
surf(NN,DD,squeeze(mc(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('mc')

figure
surf(NN,DD,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('debt tomorrow')

figure
surf(NN,DD,squeeze(cc(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('consumption')